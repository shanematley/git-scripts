# Git Utilities

A number of utilities helpful for observing a large Git repository. Currently this includes:

* git-report.sh
* update-git.sh
* send-markdown-mail.sh

## git-report.sh Script

This script checks a git repo for any changes since the last time the script was run and produces a report indicating the changes that have been made.

### Simple Usage

Create a cronjob that runs the following on a regular basis:

    git-report.sh -fm -r path/to/repo -u http://myrepo.local/cgit/branch/commit/?id=%h -a origin/trunk -a origin/release/company

The above command will perform a git fetch, check the specified repository for change to any branches starting with `origin/trunk` and `origin/release/company` and will compile a report with the list of changes in the markdown format with links to the commits on the _myrepo.local_ cgit web server.

### Options

    -h   display help
    -f  run git fetch first
    -r  the repo location
    -m  output as markdown (as oppposed to default of text)
    -t  test mode - doesn't write updated to SHAs to file
    -u  add a url to each SHA to provide a link. %h will be replaced with the SHA
    -a  add a remote reference pattern to track

### Keychain Support

If a git fetch is requested then the users private key may be required. This script has support for keychain in that it checks for a script at `$HOME/.keychain/HOSTNAME-sh` and executes it if present. For additional info: <http://www.cyberciti.biz/faq/ssh-passwordless-login-with-keychain-for-scripts/>

### State

The only state persisted by this script is a directory in the users home folder named `.git_report`. This folder contains a series of files with the SHAs each branch pointed to last time the script was run.

### Cronjob

A sample cronjob to run once a day at 7am:

    0  7  * * 1-5 ~/bin/git-report.sh -fm -r $HOME/myrepo -u http://mycgitserver.local/cgit/mycompany/commit/?id=\%h -a origin/trunk -a origin/release/mycompany | ~/bin/send-markdown-mail.sh -s "Git Repo Update Report" me@nowhere.com
    
**Note**: because of the dependencies `send-markdown-mail.sh` has you will need to set your path appropriately in cron (`man 5 crontab` to get default `PATH`). E.g. place this at the top of the crontab:

    PATH="/usr/sbin:/usr/bin:/bin"
    
**Note**: the `%h` is escaped with a backslash in the crontab entry as `%` has special meaning to cron. (`man 5 crontab` for details)

## update-git.sh Script

This script fetches from the default remote and moves all local branches to match their remote equivalents unless the branches have diverged or there are changes present in the currently checked out branch.

### Simple Usage

    update-git.sh path/to/repo

### Keychain Support

This script has keychain support. See above in section for git-report.sh for details.

### State

This script persists no state to the filesystem. (Other than the obvious git repository changes.)

## send-markdown-mail.sh Script

Emails markdown provided in stdin to the requested email address in HTML format using `sendmail`.

### Simple Usage

    echo -e '#Heading 1\nText' | send-markdown-mail.sh -s "My Message" me@nowhere.com

### Dependencies

This script requires:

* `multimarkdown` from the `libtext-multimarkdown-perl` package.
* `sendmail`

