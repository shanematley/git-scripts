#!/bin/bash
#
# A script to convert markdown to HTML with a little extra formatting
#
# Usage:
#
# echo -e "# Heading 1\nText" | send-markdown-mail.sh -s "Title" me@nowhere.com
#
# Requires packages: libtext-multimarkdown-perl

display_usage() {
    cat <<EOF
USAGE

    $(basename $0) [OPTIONS] -s "Subject" address1 address2

DESCRIPTION

    Sends a markdown formatted email taking stdin as the markdown.

SAMPLE USAGE

    echo -e "# Heading 1\nText" | $(basename $0) -s "My Message" me@nowhere.com

    or to print message without sending

    echo -e "# Heading 1\nText" | $(basename $0) -ts "My Message" me@nowhere.com

OPTIONS

    -s  Subject of the message
    -t  Test mode (print raw message without sending using sendmail

EOF
    [[ -n $1 ]] && { echo $1; exit 1; }
    exit 0
}

while getopts :hs:f:t opt; do
    case $opt in
        h) display_usage;;
        s) SUBJECT="$OPTARG";;
        f) FROM="$OPTARG";;
        t) TESTMODE=1;;
        \?) display_usage "Bad parameter -$OPTARG";;
        :) display_usage "Option -$OPTARG requires an argument that was not provided";;
    esac
done

shift $((OPTIND-1))
RECIPIENTS="${@}"
function join { local IFS="$1"; shift; echo "$*"; }
TO=$(join , ${RECIPIENTS[@]})

# Create HTML content
IN=`cat | multimarkdown`
read -r -d '' HTML <<EOF
<html>
<head>
    <style>
        body {
            font-family: Calibri, Arial, sans-serif;
            font-size: 11pt;
        }
        table {
            font-family:Arial, Helvetica, sans-serif;
            color:#666;
            font-size:12px;
            background:#eaebec;
            border:#ccc 1px solid;
        }
        table th {
            padding:1px 5px 2px 5px;

            background: #ededed;
            background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
            background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
        }
        table tr {
            text-align: center;
        }
        table td {
            padding:4;
            background: #fafafa;
            background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
            background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
        }
        h1 {
            font-size: 13pt;
            font-weight: bold;
        }
    </style>
</head>
<body>
$IN
</body>
</html>
EOF

# Create headers
HEADERS=()
HEADERS+=("To: $TO")
HEADERS+=("Subject: $SUBJECT")
[[ -n $FROM ]] && HEADERS+=("From: $FROM")
HEADERS+=("Content-Type: text/html")
HEADER=
for h in "${HEADERS[@]}"; do
    HEADER="${HEADER}${h}"$'\n'
done

# Create message with headers and HTML
read -r -d '' MESSAGE <<-EOFEOF
$HEADER
$HTML
EOFEOF

# Finally do the send or print to terminal
if [[ -z $TESTMODE ]]; then
    echo "$MESSAGE" | sendmail -t
else
    echo "$MESSAGE"
fi

