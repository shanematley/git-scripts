#!/bin/bash
# Author: Shane Matley

STORAGE_BASE="$HOME/.git_report/"
MAX_NUMBER_OF_COMMITS_IN_MERGE=10
KEYCHAIN_FILE="$HOME/.keychain/"`/bin/hostname`-sh

REPO=
REFS=()
INDENT_SIZE=2
TEST_MODE=
CGIT_BASE=

repl() { [[ $2 != 0 ]] && printf "$1"'%.s' $(eval "echo {1.."$(($2))"}"); }
get_spaces() { repl ' ' $1; }

display_usage() {
    cat <<EOF
USAGE

    $(basename $0) [OPTIONS] -r REPO_LOCATION -a REMOTE1 -a REMOTE2

DESCRIPTION

    Checks a Git repo for any changes since the last time this script was run and
    produces a report indicating the changes that have been made.

SAMPLE USAGE

    $(basename $0) -m -r ~/my_git_repo -u http://myrepo.local/cgit/branch/commit/?id=%h -a origin/trunk -a origin/release/company

OPTIONS

    -h  display help
    -f  run git fetch first
    -r  the repo location
    -m  output as markdown
    -t  test mode - doesn't write updated SHAs to file
    -u  add a url to each SHA to provide a link. %h will be replaced with the SHA
    -a  add a remote reference pattern to track

EOF
    [[ -n $1 ]] && echo $1
}

while getopts tmfhr:u:a: opt; do
    case $opt in
        \?|h) display_usage; exit 1;;
        f) RUN_GIT_FETCH=1;;
        r) REPO="$OPTARG";;
        m) USE_MARKDOWN=1;;
        t) TEST_MODE=1;;
        u) CGIT_BASE="$OPTARG";;
        a) REFS+=("refs/remotes/$OPTARG");;
    esac
done

[[ -z $REPO ]] && { display_usage; exit 1; }
[[ ${#REFS[*]} == 0 ]] && { display_usage "There are no remote refs to check. Use the -a option."; exit 1 ; }

STORAGE="${STORAGE_BASE}${REPO//\//_}"

echo "Updates to $(readlink -f $REPO) on $(hostname) as of $(date)"
echo

# Read current REMOTES from file
# Each line of the file applies to a single remote
# Format of each line is: REMOTE:REF_SHA
# E.g. refs/remotes/origin/trunk:79d2738a7191d2801c6f5703fb75bbaff0e852db
declare -A REMOTES
declare -A NEW_REMOTES
if [[ -f $STORAGE ]]; then
    oldIFS="$IFS" IFS=":"
    while read name value; do
        REMOTES[$name]="$value"
    done < "$STORAGE"
    IFS="$oldIFS"
fi

# Uncomment the next line for testing purposes
#REMOTES[refs/remotes/origin/trunk]="2540c72738a71911bead7fa9833ae13079466414"

# Check files from file against refs from repo
pushd "$REPO" >/dev/null || exit 2
trap "popd >/dev/null" EXIT

# Perform a Git fetch if requested.
if [[ $RUN_GIT_FETCH ]]; then
    # If keychain is available, use it!
    [[ -f $KEYCHAIN_FILE ]] && . "$KEYCHAIN_FILE"
    echo "Fetching from origin and pruning remotes that no longer exist"
    git --no-pager fetch -pq
fi

# Usage: print_revisions StartSha FinishSha Indent
# Print revisions in interval (StartSha, FinishSha], indenting to the requested
# number of spaces.
print_revisions() {
    local START="$1"
    local FINISH="$2"
    local INDENT=$3
    # Ensure there is at least 1 revision between START and FINISH.
    if [[ $(git rev-list --count --first-parent "$START".."$FINISH") != 0 ]]; then
        for REV in $(git rev-list --first-parent "$START".."$FINISH"); do
            # Change format depending on whether or not we are using markdown.
            if [[ $USE_MARKDOWN ]]; then
                if [[ -n $CGIT_BASE ]]; then
                    PRETTY="tformat:$(get_spaces $INDENT)* <a href='$CGIT_BASE'>%h</a> (%ar) **%an** - %s"
                else
                    PRETTY="tformat:$(get_spaces $INDENT)* %h (%ar) **%an** - %s"
                fi
            else
                PRETTY="tformat:$(get_spaces $INDENT)%h %ar <%an> %s"
            fi

            # Print out this revision
            git --no-pager log -1 --decorate --pretty="$PRETTY" --date=short $REV

            # If this is a merge (i.e. there is a second parent revision to this commit
            # then we recurse into it. If there is a lot of commits we will elide them
            # and just print how many there are, otherwise we will print them.
            if git rev-parse --verify $REV^2 &> /dev/null; then
                SECOND_PARENT=$(git rev-parse --verify $REV^2)
                NUMBER_OF_COMMITS=$(git rev-list --count $SECOND_PARENT --not $REV~)
                if (( $NUMBER_OF_COMMITS > $MAX_NUMBER_OF_COMMITS_IN_MERGE )); then
                    # Too many to show. Just print number of commits
                    if [[ $USE_MARKDOWN ]]; then
                        echo "$(get_spaces $(($INDENT+$INDENT_SIZE)))* _...eliding $NUMBER_OF_COMMITS commits from merge parent..._"
                    else
                        echo "$(get_spaces $(($INDENT+$INDENT_SIZE)))...eliding $NUMBER_OF_COMMITS commits from merge parent..."
                    fi
                else
                    # Show all the revisions by recursing back into this function with
                    # the appropriate revision start and end, and a greater indent.
                    print_revisions $REV~ $SECOND_PARENT $(($INDENT+$INDENT_SIZE))
                fi
            fi
        done
    else
        # If there are no changes then report as such.
        if [[ $USE_MARKDOWN ]]; then
            echo "$(get_spaces $INDENT)* _No Changes_"
        else
            echo "$(get_spaces $INDENT)No changes"
        fi
    fi
}

while read ref objname; do
    BRANCH="${ref/refs\/remotes\/origin\//}"
    if [[ $USE_MARKDOWN ]]; then
        echo -e "# Branch **$BRANCH**\n"
        INDENT=0
    else
        echo -e "# Branch $BRANCH\n"
        INDENT=2
    fi
    NEW_REMOTES[$ref]="$objname"
    if [[ -z ${REMOTES[$ref]} ]]; then
        echo -e "First time we've seen $ref. Displaying last 24 hours.\n"
        print_revisions "$ref@{1 day ago}" $objname $INDENT
        echo
    else
        print_revisions ${REMOTES[$ref]} $objname $INDENT
        echo
    fi
done < <(git for-each-ref --format="%(refname) %(objectname)" "${REFS[@]}")

# Write out NEW_REMOTES to file
#echo "Size of NEW_REMOTES: ${#NEW_REMOTES[*]}"
if [[ -z $TEST_MODE ]]; then
    [[ ! -d $STORAGE_BASE ]] && mkdir "$STORAGE_BASE"
    echo -n > "$STORAGE"
    for i in "${!NEW_REMOTES[@]}"; do
        echo "$i:${NEW_REMOTES[$i]}" >> "$STORAGE"
    done
fi

