#!/bin/bash

# Fetches a git repository and moves all local branches to match remotes unless
# the branches have diverged.
#
# Usage: update-git.sh PathToRepo
#
# Author: Shane Matley
# Update: 19 Dec 2013

[[ -z $1 ]] && { echo "Usage: $(basename $0) PathToRepo" >&2; exit 1; }

REPO="$1"
KEYCHAIN_FILE="$HOME/.keychain/"`/bin/hostname`-sh

# Move to REPO directory and ensure is a Git repo
pushd "$REPO" >/dev/null || { echo "Could not find repo $REPO"; exit 1; }
trap "popd >/dev/null" EXIT
git rev-parse &>/dev/null || { echo "Not a Git repo: $REPO"; exit 1; }

# If keychain is available, use it!
[[ -f $KEYCHAIN_FILE ]] && . "$KEYCHAIN_FILE"

echo "Updating branches at $REPO to match their remotes"

git fetch -q || { echo "Unable to fetch with $REPO"; exit 1; }

head="$(git symbolic-ref HEAD)"
echo "HEAD is currently: $head"

git for-each-ref --format="%(refname) %(upstream)" refs/heads | while read ref up; do
    if [[ -n $up ]]; then
        mine="$(git rev-parse "$ref")"
        theirs="$(git rev-parse "$up" 2>/dev/null)"
        if [[ $? == 0 ]]; then
            base="$(git merge-base "$ref" "$up")"

            # Check if revisions at the head of both branches are different
            if [[ $mine != $theirs ]]; then

                # Ensure we can fast-forward by verify local branch head is the merge base
                if [[ $mine == $base ]]; then

                    # Ensure the local branch is not currently checked out.
                    if [[ $ref != $head ]]; then
                        echo "Updating $ref: Mine ($mine) Theirs ($theirs) Base ($base)"
                        git update-ref "$ref" "$theirs"
                    elif git diff-index --quiet HEAD --; then
                        echo "Updating  $ref (HEAD): Mine ($mine) Theirs ($theirs) Base ($base)"
                        git merge -q --ff-only "$up"
                    else
                        echo "Not updating $ref (HEAD) as there are changes present." >&2
                    fi
                else
                    echo "Cannot update $ref as branches have diverged: Mine ($mine) Theirs ($theirs) Base ($base)" >&2
                fi
            else
                echo "Already up-to-date $ref: $mine"
            fi
        else
            echo "Cannot update $ref as remote no longer exists ($theirs)." >&2
        fi
    fi
done
